;(function() {
	
	var currentPage = 0;
	var totalBlocks = 0; 
	var blocksOnPage = 3;

	function renderHtml()
	{ 
		document.body.innerHTML += ['<div id="header">\
			<img class="icon" src="images/youtube.png" alt="lorem"></img>\
			<input type=text id="search" value="hello"/></input>\
		</div>\
		<div id="container" >\
			<div id="slider">\
			</div>\
		</div>\
		<div id="footer"><div id="pagination"></div></div>'].join("");	

		sliderEventListeners();		
		sendRequest('hello');
	}

	function calculateBlocksOnPage(countBlocks) { // determines whether display should be 1, 2 or 3 column and calculates column width
        barWidth = window.innerWidth;
        if (barWidth >= 780){
        	blocksOnPage = 3;
        	return;
        }            
        if(barWidth > 600)
        	blocksOnPage = 2;
        else
        	blocksOnPage = 1; 
    }

	function renderPages()
	{
		var pageBlock = document.getElementById('pagination');
		calculateBlocksOnPage(totalBlocks);

		if(totalBlocks <= 0){
			pageBlock.innerHTML = '';
			document.getElementById('slider').innerHTML = '<div class="no-results">No results</div>';
			return ;
		};

		pageBlock.innerHTML = '<a class="tooltip"><div class="page-circle active"><span>1</span></div></a>';

		for (var i = 2; i < parseInt((totalBlocks+blocksOnPage-1)/blocksOnPage) + 1; i++) {
			pageBlock.innerHTML += '<a class="tooltip"><div class="page-circle"><span>' + i +'</span></div></a>';
		};
		pageListeners();
	}


	function pageListeners()
	{
		currentPage = 0;

		var pages = document.getElementsByClassName('page-circle');
		lastPage = pages.length; 

		for(var i = 0; i < lastPage ; i++){			
    		pages[i].addEventListener('click', function(ev){
    			var pageNumber = parseInt(this.firstChild.outerText);
				switchToPage( pageNumber - 1 );
    		}, false);
		}				
	}

	function switchToPage(pageNumber)
	{
		var pages = document.getElementsByClassName('page-circle');	 

		if (pageNumber < 0)
			pageNumber = 0;
		if(pageNumber >= pages.length - 1)
			pageNumber = pages.length - 1; 

		pages[currentPage].className = 'page-circle'; 
		pages[pageNumber].className = 'page-circle active';
		currentPage = pageNumber;

		transformSlider(-parseFloat(document.body.offsetWidth) * pageNumber);		  	
	}

	function sliderEventListeners(startPage)
	{
		var slider = document.getElementById('slider'); 
		var style = window.getComputedStyle(slider);
		var width = 0 ;  
		var positionX=0; 
		var startX = 0;
		var sliderPress = false;		

		var searchInput = document.getElementById('search');
		searchInput.addEventListener('keypress', function (ev) {
			if (ev.which == 13) {
				sendRequest(searchInput.value);
			}
		});

		slider.addEventListener('mousedown', function(ev){
			width = parseFloat(document.body.offsetWidth) ;
			sliderPress = true;
			positionX = ev.clientX;
			startX = ev.clientX;
			slider.addEventListener('mousemove', mouseMoveHandler ,false);			
		}); 

		window.addEventListener('resize', function (ev){ 
			var oldCountOnPage =  blocksOnPage ;
			var oldBlockIndex = oldCountOnPage *currentPage ; 
			renderPages();
			var newPage = parseInt((oldBlockIndex -blocksOnPage +1)/(blocksOnPage));
			switchToPage(newPage);			

		});

		window.addEventListener('mouseup', mouseUpHandler, true);  

		function mouseUpHandler(ev)
		{
			if(!sliderPress)
				return;
			var page = currentPage;			
			var totalOffset = ev.clientX - startX; // (totalOffset); 
			if( Math.abs(totalOffset) > width/3){
				if(totalOffset < 0 ){
					switchToPage(currentPage + 1);
				}
				else{
					switchToPage(currentPage - 1);
				}				
			} 
			slider.style.left = -currentPage*width + 'px' ;
			slider.removeEventListener('mousemove',mouseMoveHandler,false); 
			sliderPress = false;
		}

		function mouseMoveHandler(ev)
		{ 
			slider.style.webkitTransition = ''; 
			var offset = ev.clientX - positionX;
			var left = parseFloat(style.left); 
			positionX = ev.clientX;
			left += offset;		
			slider.style.left = left + 'px';
		} 			
				
	}

	function transformSlider(lefthPosition)
	{
		var slider = document.getElementById('slider'); 
		slider.style.webkitTransition = 'left 1s' ; 
		slider.style.left = lefthPosition + 'px'; 
	} 	

	window.onload = renderHtml;

	function getXmlHttpRequest(){
		var xmlhttp;
		try{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e){
			try {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		    } 
		    catch (E) {
		      xmlhttp = false;
		    }
		}

		if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
		    xmlhttp = new XMLHttpRequest();
		}
		return xmlhttp;
	}	

	function sendRequest(query)
	{
		var httpRequest = getXmlHttpRequest();
		httpRequest.open('GET', 
			'http://gdata.youtube.com/feeds/api/videos/?alt=json&max-results=15&start-index=1&q='+ query, true);
		httpRequest.onreadystatechange = function() {
		  if (httpRequest.readyState != 4){
		  	return;
		  }
		  if(httpRequest.status == 200) {
		  	renderClipBlock(convertYouTubeResponseToClipList( JSON.parse(httpRequest.responseText)));
		  }	
		};
		httpRequest.send(null);
	}

	function renderClipBlock(data)
	{
		var slider = document.getElementById('slider'); 

		var source   = document.getElementById('clipTemplate').innerHTML.toString();		
		var clipBlockTemplate = Handlebars.compile(source);
		slider.innerHTML = clipBlockTemplate( {'data' : data });

		renderPages();

		switchToPage(0) ;
	}

	function convertYouTubeResponseToClipList(rawYouTubeData) {
		var clipList = [];
		var entries = rawYouTubeData.feed.entry;
		if (entries) {
			for (var i = 0, l = entries.length; i < l; i++){
			var entry = entries[i];

			var date = new Date(Date.parse(entry.updated.$t));
			var shortId = entry.id.$t.match(/videos.*/).toString().split("/")[1];
			clipList.push({
			id: shortId,
			youtubeLink: "http://www.youtube.com/watch?v=" + shortId,
			title: entry.title.$t,
			thumbnail: entry.media$group.media$thumbnail[1].url,
			description: entry.media$group.media$description.$t,
			author: entry.author[0].name.$t,
			publishDate: date.toUTCString(),
			viewCount: entry.yt$statistics.viewCount
			});
			}
		}
		totalBlocks = clipList.length;
		return clipList;
	}

	function handleError(message) {
		alert("Error: "+message);
	}

	
}());
